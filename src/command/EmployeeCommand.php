<?php

namespace App\command;

use App\model\AbstractEmployee;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class EmployeeCommand  extends Command
{

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritdoc}
     */
    protected static $defaultName = 'company:employee';

    /**
     * {@inheritdoc}
     */
    public function __construct(ContainerInterface $container, string $name = null)
    {
        parent::__construct($name);
        $this->container = $container;
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->addArgument('employee', InputArgument::REQUIRED, 'Employee`s profession');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $employee = $input->getArgument('employee');
        /** @var AbstractEmployee|null $employee */
        $employee = $this->container->get($employee, ContainerInterface::NULL_ON_INVALID_REFERENCE);
        if (null !== $employee) {
            foreach ($employee->getJobs() as $job) {
                $output->writeln('- ' . $job::getJobName());
            }

            return 0;
        }

        $output->writeln('Employee not exist');
        return 1;
    }
}
