<?php

namespace App\command;

use App\model\AbstractEmployee;
use App\model\AbstractJob;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class JobCommand  extends Command
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritdoc}
     */
    protected static $defaultName = 'employee:can';

    /**
     * {@inheritdoc}
     */
    public function __construct(ContainerInterface $container, string $name = null)
    {
        parent::__construct($name);
        $this->container = $container;
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->addArgument('employee', InputArgument::REQUIRED, 'Employee`s profession')
            ->addArgument('job', InputArgument::REQUIRED, 'Employee`s job');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $employee = $input->getArgument('employee');
        $job = $input->getArgument('job');
        /** @var AbstractEmployee|null $employee */
        $employee = $this->container->get($employee, ContainerInterface::NULL_ON_INVALID_REFERENCE);
        /** @var AbstractJob|null $job */
        $job = $this->container->get($job, ContainerInterface::NULL_ON_INVALID_REFERENCE);
        if (null !== $employee && null !== $job) {
            $output->writeln($employee->can($job) ? 'true' : 'false');

            return 0;
        }

        $output->writeln('Employee or job not exist');
        return 1;
    }
}
