<?php

namespace App\model;

class CommunicationWithManagerJob extends AbstractJob
{
    /**
     * {@inheritDoc}
     */
    static public function getJobName(): string
    {
        return 'communication with manager';
    }
}
