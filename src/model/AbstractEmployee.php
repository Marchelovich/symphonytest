<?php

namespace App\model;

abstract class AbstractEmployee
{
    /** @var AbstractJob[] */
    private $jobs;

    /**
     * AbstractEmployee constructor.
     * @param array $jobs
     */
    public function __construct(array $jobs)
    {
        $jobs = array_filter($jobs, function (AbstractJob $job): bool {
            return $job instanceof  AbstractJob;
        });
        $this->jobs = $jobs;
    }

    /**
     * @return AbstractJob[]
     */
    public function getJobs(): array
    {
        return $this->jobs;
    }

    /**
     * @param AbstractJob $job
     * @return bool
     */
    public function can(AbstractJob $job): bool
    {
        foreach ($this->jobs as $existJob) {
            if ($existJob::getJobName() === $job::getJobName()) {
                return true;
            }
        }

        return false;
    }

    /**
     * @return string
     */
    static abstract public function getEmployeeName(): string;
}
