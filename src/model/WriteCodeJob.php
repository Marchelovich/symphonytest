<?php

namespace App\model;

class WriteCodeJob extends AbstractJob
{
    /**
     * {@inheritDoc}
     */
    static public function getJobName(): string
    {
        return 'write code';
    }
}
