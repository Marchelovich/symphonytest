<?php

namespace App\model;

class TestCodeJob extends AbstractJob
{
    /**
     * {@inheritDoc}
     */
    static public function getJobName(): string
    {
        return 'test code';
    }
}
