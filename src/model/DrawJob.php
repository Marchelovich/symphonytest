<?php

namespace App\model;

class DrawJob extends AbstractJob
{
    /**
     * {@inheritDoc}
     */
    static public function getJobName(): string
    {
        return 'draw code';
    }
}
