<?php

namespace App\model;

class Manager extends AbstractEmployee
{
    /**
     * {@inheritDoc}
     */
    static public function getEmployeeName(): string
    {
        return 'manager';
    }
}
