<?php

namespace App\model;

class Programmer extends AbstractEmployee
{
    /**
     * {@inheritDoc}
     */
    static public function getEmployeeName(): string
    {
        return 'programmer';
    }
}
