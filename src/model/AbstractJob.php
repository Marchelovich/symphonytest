<?php

namespace App\model;

abstract class AbstractJob
{
    /**
     * @return string
     */
    static abstract public function getJobName(): string;
}