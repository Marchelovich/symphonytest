<?php

namespace App\model;

class Tester extends AbstractEmployee
{
    /**
     * {@inheritDoc}
     */
    static public function getEmployeeName(): string
    {
        return 'tester';
    }
}
