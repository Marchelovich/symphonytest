<?php

namespace App\model;

class SetTasksJob extends AbstractJob
{
    /**
     * {@inheritDoc}
     */
    static public function getJobName(): string
    {
        return 'set tasks';
    }
}
