<?php

namespace App\model;

class Designer extends AbstractEmployee
{
    /**
     * {@inheritDoc}
     */
    static public function getEmployeeName(): string
    {
        return 'designer';
    }
}
